let tasks = {
    "WRITE": 1,
    "DELETE": 2
}

let descriptors = [
    'engineer',
    'developer',
    'traveler',
    'early bird',
    'music lover',
    'coffee snob',
    'left handed',
    'techie',
    'learner',
    'designer',
    'foodie',
    'human',
    'robot',
    '6ft. 2in.',
    'hiker',
    'influencer',
    'closer',
    'opener',
    'hat wearer',
    'musician',
    'explorer',
    'grillmaster',
    'creator',
    'inventor',
    'dreamer',
    'thinker',
    'cool dude',
    'cool guy',
    'captain',
    'doer',
    'cord cutter',
    'friend',
    'weird',
    'rocketeer'
]

let projects = [
    {
        title: 'Zoom',
        images: [
            'res/zoom/flightsearch.jpg',
            'res/zoom/flightstatus.jpg',
            'res/zoom/flightdetails.jpg',
            'res/zoom/flightsout.jpg',
            'res/zoom/solomap.jpg',
            'res/zoom/comparemap.jpg',
            'res/zoom/friends.jpg',
            'res/zoom/myflights.jpg',
        ],
        description: 'Travel site tailored specifically for whatever the "power user" equivalent is to travel. Zoom allows the user to quickly look up flight options and details to plan a trip. It also has a social aspect where you can record travel and compare with friends.',
        footer: 'Node.js, Express, mongoDB'
    },
    {
        title: 'Howlr Retro Board',
        images: [
            'res/howlr/board.jpg',
            'res/howlr/homePage.jpg',
            'res/howlr/newBoard.jpg',
        ],
        description: 'An open source retro board that allows agile teams to discuss previous sprints. Notes can be pinned to the board under user-defined columns such as "Did Well", and "Action Items". This project is open source and the source code can be found <a href="https://gitlab.com/Haydo/howlr-retro-board">here</a>.',
        footer: 'Docker, Node.js, Express, mongoDB'
    },
    {
        title: 'Earbuds',
        images: [
            'res/tbd.jpg'
        ],
        description: 'Music listening app with the intention of listening to music with others on Spotify. Users enter a listening room (like a chat room) and Earbuds will sync all present user\'s Spotify players.',
        footer: 'Node.js, Express, mongoDB, Websockets'
    },
    {
        title: 'Citrus Delivery',
        images: [
            'res/citrus/order.jpg',
            'res/citrus/details.jpg',
            'res/citrus/payment.jpg'
        ],
        description: 'Delivery app geared towards high desnity living in urban areas. Launched at UTD, but has since been disconinued. <a rel="noopener" target="_blank" href="https://utdmercury.com/eating-more-for-less/">Read More</a>',
        footer: 'Node.js, Express, mongoDB, Twilio, Stripe'
    },
    {
        title: 'Haystack',
        images: [
            'res/haystack/main.jpg',
            'res/haystack/newconnect.jpg',
            'res/haystack/auth.jpg',
            'res/haystack/searchRule.jpg',
            'res/haystack/goodfilter.jpg',
            'res/haystack/badfilter.jpg'
        ],
        description: 'LDAP explorer written to make my life easier at one of my jobs. I was using <a rel="noopener" target="_blank" href="https://directory.apache.org/studio/">Apache Directory Studio</a> but found it cumbersome for most of what I needed. Haystack was written to be lightweight and efficient for searching/modifying records in an LDAP directory. It allows multiple connections and results to be pulled with a tabbed interface. I also introduced "Search Rules" to query against multiple attributes at once.',
        footer: 'Node.js, Electron, Credential Managers'
    },
    {
        title: 'haydenives.com',
        images: [
            'res/site/homepage.jpg',
        ],
        description: 'This very website! Made in house. Just me and my text editor.',
        footer: 'HTML, CSS, JavaScript, Love'
    },
    {
        title: 'JaPractice',
        images: [
            'res/tbd.jpg'
        ],
        description: 'Simple site to learn simple Japanese. It covers hirigana/katakana as well as <a rel="noopener" target="_blank" href="https://www.jlpt.jp/e/about/levelsummary.html">N5/4</a> vocabulary/kanji. Work in progress. I\'ll add a link once it\'s a little more polished!',
        footer: 'HTML, CSS, JavaScript'
    },
    {
        title: 'Prism',
        images: [
            'res/tbd.jpg',
        ],
        description: 'Utility to backup files in a one-to-many fashion. File watchers look for updates on watched files and copy the changes to specified target locations.',
        footer: 'Java'
    }
]