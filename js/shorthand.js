function qs(s) {
    return document.querySelector(s)
}

function qsa(s) {
    return document.querySelectorAll(s)
}

function id(s) {
    return document.getElementById(s)
}

function cn() {
    return document.getElementsByClassName(s)
}

function tn() {
    return document.getElementsByTagName(s)
}

function log(s) {
    if(typeof(s) === 'object') {
        console.log(s)
        return
    }
    let style = 
    'background: radial-gradient(circle, rgba(255,255,255,0.1), rgba(0,0,0,0.1)), linear-gradient(135deg, #8b84ff, #40dfdf);' +
    'background: -webkit-linear-gradient(135deg, #8b84ff, #40dfdf);' + 
    'color:black; font-size: 0.8rem; font-weight: bold; border-radius:16px;' +
    'padding: 4px; margin: 4px;' 
    console.log(`%c${s}`, style)
}

function error(s) {
    if(typeof(s) === 'object') {
        console.log(s)
        return
    }
    let style = 
    'background: radial-gradient(circle, rgba(255,255,255,0.1), rgba(0,0,0,0.1)), linear-gradient(135deg, #8b84ff, #40dfdf);' +
    'background: -webkit-linear-gradient(135deg, #8b84ff, #40dfdf);' + 
    'color:black; font-size: 0.8rem; font-weight: bold; border-radius:16px;' +
    'padding: 4px; margin: 4px;' 
    console.error(`%c${s}`, style)
}